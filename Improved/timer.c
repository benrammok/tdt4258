#include <stdint.h>
#include <stdbool.h>

#include "efm32gg.h"
  
/*
 * Function to setup the timer
 * Period in Cycles 
 */ 
void setupTimer (uint16_t period) 
{
  *CMU_HFPERCLKEN0 |= CMU2_HFPERCLKEN0_TIMER1;
  *TIMER1_TOP = period; 
  *TIMER1_IEN = 1; 
  *TIMER1_CMD = 1;
} 
 
/*
 * Function to start the timer
 */ 
void startTimer ()
{ 
  *TIMER1_CMD = 1;
} 

/*
 * Function to stop the timer
 */  
void stopTimer ()
{  
  *TIMER1_CMD = 2;
} 

/*
 * Function to disable the timer 
 */  
void disableTimer ()
{ 
  *TIMER1_TOP = 0; 
  *TIMER1_IEN = 0;
  *TIMER1_CMD = 2;
  *CMU_HFPERCLKEN0 &= ~CMU2_HFPERCLKEN0_TIMER1;
} 
