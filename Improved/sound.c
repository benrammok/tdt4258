#include "sound.h"
/*
 * Peripheral Functions
 */
void stopTimer();
void disableTimer();

static uint16_t currentPos;
static uint16_t currentSample;
static uint16_t currentDur;
static uint16_t numSamples;
static uint16_t noteDuration;
static uint16_t noteDurationDelay;
static uint8_t currentRepeat;

 
/**
 * Function to initialize values
 *
*/ 
void setupSound (void) 
{ 
isPlaying = false;  
currentSample = 0;  
currentDur = 0;
currentPos = 0;
currentRepeat = 0;
numSamples = 0;
noteDuration = 0; 
noteDurationDelay = 0;
} 
 
 

/**
 * Function for playing Sounds
 * 
*/ 
void playSound (music * m)
{
  if (isPlaying)
  {
    //If we have defined a value for repeat, set currentPos = 0 and increase currentRepeat
    if (currentPos == m->length && currentRepeat != m->repeat)
    {
      currentPos = 0;	  
      currentRepeat++;	  
      //Has the song reached the end? And has it repeated, if defined?
    }
    else if (currentPos == m->length && currentRepeat == m->repeat)
    {	  
      isPlaying = false;	  
      *GPIO_PA_DOUT = 0xFF00;	  
      stopTimer ();	
    }
    else
    {
      if((m->notes + currentPos) != NULL || (m->duration + currentPos) != NULL){	  
        playNotes (m);	
      }else
      {
        currentPos = m->length; 
      }
    }
  }
}


 
void playNotes (music * m)
{
  //We only need to calculate the noteDuration and numberOfSamples once per note
  uint16_t note = *(m->notes + currentPos);
  uint16_t duration = *(m->duration + currentPos);
  
  if (note > 0 && currentDur == 0)
  {      
    numSamples = (uint16_t) (SAMPLING_FREQUENCY / note);
    noteDuration = (m->tempo / (duration * 1000.0f)) * SAMPLING_FREQUENCY;  
    noteDurationDelay = noteDuration * m->delay_after; 
    //Is the note 0? If so use that as a delay
  }
  else if (note == 0 && currentDur == 0)
  {    
    numSamples = 0;    
    noteDuration = (m->tempo / (duration * 1000.0f)) * SAMPLING_FREQUENCY;
    noteDurationDelay = noteDuration * m->delay_after;   
  }
  
  
  /*
  * Square Wave Generation
  */ 
  if (numSamples > 0)
  {    
    *GPIO_PA_DOUT = ~(*(m->notes + currentPos) << 8);     
    if (currentSample > numSamples / 2)
    {	  
      *DAC0_CH0DATA = 0;	  
      *DAC0_CH1DATA = 0;	
    }
    else
    {	  
      *DAC0_CH0DATA = VOLUME;	  
      *DAC0_CH1DATA = VOLUME;	
    }
    currentSample++;  
  }
  else
  {     
    *DAC0_CH0DATA = 0;     
    *DAC0_CH1DATA = 0;   
  }
  
  //Start the note over when currentSample has reached numSamples
  if (currentSample > numSamples && currentDur <= noteDuration)
  {     
    currentSample = 0;   
  }
  
  
  currentDur++;
  
  //Go to next note after noteDuration has been reached + some delay
  if (currentDur >= noteDurationDelay)
  {     
    currentPos++;     
    currentDur = 0;  
  }
}
