#include <stdint.h>
#include <stdbool.h>
#include "efm32gg.h"
#include "sound.h"

#define SAMPLE_PERIOD 317
  
/*
 * Declaration of peripheral setup functions 
 */ 
void setupTimer (uint32_t period);
void setupDAC ();
void setupNVIC ();
void setupGPIO ();
void enableDeepSleep ();    /* Function for Enabling Deep Sleep */
extern bool isPlaying;      /* Playing sound or not?            */
extern music * currentSong; /* Reference to currentSong in      */
                            /* Interrupt Handler                */
 
int main (void) 
{
 /*
  * Call the peripheral setup functions 
  */ 
  setupGPIO (); 
  setupDAC ();
  setupTimer (SAMPLE_PERIOD);
 
 /*
  * Ensure all variables used in playback
  * is set to zero
  */
  setupSound ();

 /*
  * Enable interrupt handling 
  */ 
  setupNVIC ();
  
 /*
  * Play Intro Music
  */
  currentSong = &intro;
  isPlaying = true;

 /*
  * Enable Deepsleep
  */ 
  while (1)
  {    
    if (!isPlaying)
    {
      enableDeepSleep ();
    }
  };  
  return 0;
}


 
void setupNVIC () 
{  
  *ISER0 = 0x1802;		/* Enable Interrupt for GPIO & TIMER1 */
} 
 
void enableDeepSleep ()
{ 
  *SCR = 6;			    /* 6 Deep Sleep                       */ 
                    /* Assembly code Wait for Interrupt   */
  __asm ("wfi");
} 
 
 
/*
 * if other interrupt handlers are needed, use the following names:
 * NMI_Handler HardFault_Handler MemManage_Handler BusFault_Handler
 * UsageFault_Handler Reserved7_Handler Reserved8_Handler
 * Reserved9_Handler Reserved10_Handler SVC_Handler DebugMon_Handler
 * Reserved13_Handler PendSV_Handler SysTick_Handler DMA_IRQHandler
 * GPIO_EVEN_IRQHandler TIMER0_IRQHandler USART0_RX_IRQHandler
 * USART0_TX_IRQHandler USB_IRQHandler ACMP0_IRQHandler ADC0_IRQHandler
 * DAC0_IRQHandler I2C0_IRQHandler I2C1_IRQHandler GPIO_ODD_IRQHandler
 * TIMER1_IRQHandler TIMER2_IRQHandler TIMER3_IRQHandler
 * USART1_RX_IRQHandler USART1_TX_IRQHandler LESENSE_IRQHandler
 * USART2_RX_IRQHandler USART2_TX_IRQHandler UART0_RX_IRQHandler
 * UART0_TX_IRQHandler UART1_RX_IRQHandler UART1_TX_IRQHandler
 * LEUART0_IRQHandler LEUART1_IRQHandler LETIMER0_IRQHandler
 * PCNT0_IRQHandler PCNT1_IRQHandler PCNT2_IRQHandler RTC_IRQHandler
 * BURTC_IRQHandler CMU_IRQHandler VCMP_IRQHandler LCD_IRQHandler
 * MSC_IRQHandler AES_IRQHandler EBI_IRQHandler EMU_IRQHandler 
 */ 
