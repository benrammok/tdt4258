#ifndef MUSIC_STRUCT
#define MUSIC_STRUCT
#include <stdint.h>
/*
 *
 * Custom Structure to Hold parameters relating to Music/Sounds
 * Holds pointers to note and duration array, tempo, size
 * 
 */  

typedef struct Music
{
  uint16_t * notes; 
  uint16_t * duration;
  uint16_t tempo; 
  uint16_t length;  
  float delay_after;  
  uint8_t repeat;
} music;

#endif
