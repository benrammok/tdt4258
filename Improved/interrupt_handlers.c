#include <stdint.h>
#include <stdbool.h>
#include "efm32gg.h"
#include "sound.h"

/*Preprocessor Macro for determing which button is pressed
 * Uses the labeling on the gamepad. So NUM = 1 is bit 0 of DIN
 */ 
#define BUTTON_PRESSED(NUM) !((*GPIO_PC_DIN >> (NUM-1)) & 0x1)
  
/*
 * Global Variables
 */
extern bool isPlaying;
music * currentSong;   /*Pointer for Adress of Current Song*/

/*
 * Peripheral Functions
 */
void setupTimer (uint16_t);
void setupSound ();
void buttonPressedHandler (void);
void enableDeepSleep ();
void stopTimer();
void startTimer();
void setupDAC();


/*
 * TIMER1 interrupt handler 
 */ 
  
void __attribute__ ((interrupt)) TIMER1_IRQHandler () 
{
  *TIMER1_IFC = 1;		// Clear Interrupt
  playSound (currentSong);
} 
 

/*
 * GPIO even pin interrupt handler 
 */

void __attribute__ ((interrupt)) GPIO_EVEN_IRQHandler () 
{
  *GPIO_IFC = *GPIO_IF; 
  buttonPressedHandler ();
} 
 

/*
 * GPIO odd pin interrupt handler 
 */ 
void __attribute__ ((interrupt)) GPIO_ODD_IRQHandler () 
{  
  *GPIO_IFC = *GPIO_IF; 
  buttonPressedHandler ();
} 
 
/*
 * Function for Disabling Deep Sleep 
 */  
void disableDeepSleep ()
{
  *SCR = 0;
} 

/*
 * Function for handling button press 
 */ 
void buttonPressedHandler ()
{  
  //Is sounds playing? 
  if (!isPlaying)
  {
    disableDeepSleep ();
    setupTimer (SAMPLE_PERIOD);  
  }
  else
  {
    stopTimer();
  }

  //Ensure all variables is cleared before playback
  setupSound ();
    
  //Pin 0 is conencted to SW1 on the Gamepad 
  if (BUTTON_PRESSED (1))
  {      
    currentSong = &win;    
    isPlaying = true;
    startTimer();   
  }

  //Pin 1 is conencted to SW2 on the Gamepad 
  if (BUTTON_PRESSED (2))
  {     
    currentSong = &hit;     
    isPlaying = true;
    startTimer();    
  }

  //Pin 2 is conencted to SW3 on the Gamepad 
  if (BUTTON_PRESSED (3))
  {     
    currentSong = &loss;     
    isPlaying = true; 
    startTimer();   
  }
}


