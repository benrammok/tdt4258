#include "sound_effect.h"
/*
 * Sample Arrays
 * Very Simple Sounds Created on https://onlinesequencer.net/
 */ 
static uint16_t intro_melody[] ={NOTE_C4, 0, NOTE_D4, NOTE_A3,NOTE_D4, 0, NOTE_A3, NOTE_B3, 0, NOTE_C4, 0, NOTE_A3, NOTE_D4};
static uint16_t intro_dur[] = { 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4 };
 
static uint16_t hit_melody[] = { NOTE_C3, NOTE_D2, NOTE_E2 };
static uint16_t hit_dur[] = { 32, 60, 38 };

static uint16_t loss_melody[] = { NOTE_B2, 0, NOTE_AS2, 0, NOTE_GS2, 0, NOTE_FS2, 0, NOTE_E2};
static uint16_t loss_dur[] = { 4, 4, 8, 8, 8, 8, 8, 8, 4};

static uint16_t win_melody[] = { NOTE_B4, 0, NOTE_B4, 0, NOTE_CS5, 0, NOTE_D5, NOTE_E5, 0, NOTE_FS5, 0, NOTE_E5};
static uint16_t win_dur[] = { 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4};

/*
 * Structures for holding properties for each sound
 */
music hit =
{
  hit_melody, 
  hit_dur, 
  1000, 
  sizeof (hit_melody) / sizeof (hit_melody[0]),   
  2.0f, 
  0 
};

music intro =
{
  intro_melody, 
  intro_dur, 
  400,   
  sizeof (intro_melody) / sizeof (intro_melody[0]), 
  1.3f, 
  0 
};

music win =
{
  win_melody, 
  win_dur, 
  400,   
  sizeof (win_melody) / sizeof (win_melody[0]), 
  1.3f, 
  0
};

music loss =
{
  loss_melody, 
  loss_dur, 
  400,   
  sizeof (loss_melody) / sizeof (loss_melody[0]), 
  1.3f, 
  0
};