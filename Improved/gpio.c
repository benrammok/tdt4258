#include <stdint.h>
#include <stdbool.h>

#include "efm32gg.h"
  
/*
 * Function to set up GPIO mode and interrupts
 */ 
void setupGPIO () 
{
  *CMU_HFPERCLKEN0 |= CMU2_HFPERCLKEN0_GPIO;	/* enable GPIO clock        */
  *GPIO_PA_CTRL = 3;	                      	/* set 1mA drive strength   */
  *GPIO_PA_MODEH = 0x55555555;	              /* set pins A8-15 as output */ 
  *GPIO_PA_DOUT = 0xFF00;	                    /* turn off LEDs D1-D8      */

  /*
  * Button Configuration
  */  
  *GPIO_PC_MODEL = 0x33333333;	             /* Input with filter        */
  *GPIO_PC_DOUT = 0xFF;		                   /* Turn on pull-up          */

  /*
  * Interrupt Setup for Buttons
  */  
  *GPIO_EXTIPSELL = 0x22222222;                /* Enable Interrupt for Port C */
  *GPIO_EXTIFALL = 0xFF;	                     /* Interrupt on falling edge   */
  *GPIO_IFC = *GPIO_IF;                        /* Clear Interrupt             */
                                               /* just to be safe             */
  *GPIO_IEN = 0xFF;                            /* Enable Interrupt            */
} 
