#ifndef SOUND_EFFECT
#define SOUND_EFFECT
#include "notes.h"
#include "music_struct.h"
#include <stdint.h>
//External Declaration of Struct in sound_effect.c file
//Custom Created Sounds
extern music intro;
extern music hit;
extern music win;
extern music loss;
#endif
