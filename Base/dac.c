#include <stdint.h>
#include <stdbool.h>

#include "efm32gg.h"

/*
 * Function to set up DAC
 */ 
void setupDAC () 
{
  *CMU_HFPERCLKEN0 |= CMU2_HFPERCLKEN0_DAC0;	/*Enable High Frequency Clock for DAC */
  *DAC0_CTRL = 0x50010;	                      /*Prescale DAC clock                  */
  *DAC0_CH0CTRL = 1;                          /*Enable Channel 0 of DAC             */
  *DAC0_CH1CTRL = 1;                          /*Enable Channel 1 of DAC             */
} 
 

/*
 * Function to disable DAC
 */ 
void disableDAC ()
{
  *DAC0_CTRL = 0;	                            /*Prescale DAC clock                   */
  *DAC0_CH0CTRL = 0;                          /*Disable Channel 0 of DAC             */
  *DAC0_CH1CTRL = 0;                          /*Disable Channel 1 of DAC             */ 
  *CMU_HFPERCLKEN0 &= ~CMU2_HFPERCLKEN0_DAC0;	/*Disable High Frequency Clock for DAC */
}
