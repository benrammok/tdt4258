#ifndef SOUND_H
#define SOUND_H
#include "efm32gg.h"
#include <stdint.h>
#include <stdbool.h>
#include "stdlib.h"
//Include the Notes, the sound effects, and the custom structure Music
#include "notes.h"
#include "music_struct.h"
#include "sound_effect.h"
  
/*
 * The period between sound samples, in clock cycles 
 * 317 - 44,1kHz, 73- 192kHz
 */ 
#define SAMPLE_PERIOD 317
#define VOLUME 40
#define SAMPLING_FREQUENCY F_CPU/SAMPLE_PERIOD
  
bool isPlaying;

 
 
void setupSound ();

void playSound (music *);

void playNotes (music *);

 
#endif
